unit mainwindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
   Grids, EulerMethod, HeunMethod, RungeKMethod, DormandPMethod, BaseMethod, graph;

type

   { TForm1 }

   TForm1 = class(TForm)
      btnOperate: TButton;
      btnGraphic: TButton;
      cbxMethods: TComboBox;
      edtinitialY: TEdit;
      edtXFind: TEdit;
      edtinitialX: TEdit;
      edtFunction: TEdit;
      lblMethod: TLabel;
      lblXFind: TLabel;
      lblinitialX: TLabel;
      lblFunction: TLabel;
      lblInitialY: TLabel;
      stgData: TStringGrid;
      procedure btnGraphicClick(Sender: TObject);
      procedure btnOperateClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
   private
      valsX : array of real;
      valsY : array of real;
      euler : TEulerMethod;
      heun : THeunMethod;
      runge : TRungeKuttaMethod;
      dormand : TDormandPrinceMethod;
      procedure CleanStringGrid();

   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnOperateClick(Sender: TObject);
var
   initX, inity, xfind, currentX: real;
   xyRes : TArrxy;
   i, tableRow : integer;
begin
   if (edtFunction.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese una función.');
      exit;
   end;
   if (edtinitialX.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese un x inicial.');
      exit;
   end;
   if (edtXFind.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el x a buscar.');
      exit;
   end;
   if (edtinitialY.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese un y inicial.');
      exit;
   end;
   self.CleanStringGrid();
   setLength(valsX, 0);
   setLength(valsY, 0);

   initX := strToFloat(edtinitialX.text);
   inity := strToFloat(edtinitialY.text);
   xfind := strToFloat(edtXFind.text);

   if (cbxMethods.ItemIndex = 0) then
   begin // EULER
      euler.setData(edtFunction.text, initX, initY, xfind);
      xyRes := euler.solve();
   end
   else if (cbxMethods.ItemIndex = 1) then
   begin // HEUN
      heun.setData(edtFunction.text, initX, initY, xfind);
      xyRes := heun.solve();
   end
   else if (cbxMethods.ItemIndex = 2) then
   begin // RUNGE KUTTA
      runge.setData(edtFunction.text, initX, initY, xfind);
      xyRes := runge.solve();
   end
   else if (cbxMethods.ItemIndex = 3) then
   begin // DORMAND-PRINCE
      dormand.setData(edtFunction.text, initX, initY, xfind);
      xyRes := dormand.solve();
   end;

   i := 0;
   tableRow := 1;

   while (i < length(xyRes[0])) do
   begin
      stgData.insertRowWithValues(tableRow,[]);
      stgData.Cells[0,tableRow] := IntToStr(i);
      stgData.Cells[1,tableRow] := FloatToStr(xyRes[0][i]);
      stgData.Cells[2,tableRow] := FloatToStr(xyRes[1][i]);
      tableRow := tableRow + 1;

      i := i + 1;
   end;
   frmGraficadora.setData(xyRes);
end;

procedure TForm1.btnGraphicClick(Sender: TObject);
begin
    frmGraficadora.show;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   euler := TEulerMethod.create();
   heun := THeunMethod.create();
   runge := TRungeKuttaMethod.create();
   dormand := TDormandPrinceMethod.create();

   cbxMethods.Items.Add('Euler');
   cbxMethods.Items.Add('Heun');
   cbxMethods.Items.Add('Runge-Kutta');
   cbxMethods.Items.Add('Dormand-Prince');
   cbxMethods.ItemIndex := 0;

end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   euler.destroy();
   heun.destroy();
   runge.destroy();
   dormand.destroy();
end;

procedure TForm1.CleanStringGrid();
var
   I: Integer;
begin
   for I := 0 to  stgData.ColCount - 1 do
     stgData.Cols[I].Clear;
   stgData.RowCount := 1;
end;

end.
