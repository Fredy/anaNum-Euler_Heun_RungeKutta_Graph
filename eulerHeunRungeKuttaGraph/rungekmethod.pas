unit RungeKMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, BaseMethod;

type
   TRungeKuttaMethod = class(TBaseMethod)
   public
      function solve() : TArrxy; override;
   end;

implementation
function TRungeKuttaMethod.solve() : TArrxy;
var
   xyValues : TArrxy;
   i : integer;
   xi, yi : real;
   h_2, k1, k2, k3 , k4, m : real;
begin
   setLength(xyValues, 2, 1); // 2 x N;
   xyValues[0][0] := _x0;
   xyValues[1][0] := _y0;
   i := 0;
   while (xyValues[0][i] + _h <= _xf) do
   begin
      xi := xyValues[0][i];
      yi := xyValues[1][i];

      h_2 := _h / 2;
      k1 := _function.solveSavedExpression(['x', 'y'],[xi, yi])[0][0];
      k2 := _function.solveSavedExpression(['x', 'y'],[xi + h_2, yi + k1 * h_2])[0][0];
      k3 := _function.solveSavedExpression(['x', 'y'],[xi + h_2, yi + k2 * h_2])[0][0];
      k4 := _function.solveSavedExpression(['x', 'y'],[xi + _h, yi + k3 * _h])[0][0];
      m := (k1 + 2*k2 + 2*k3 + k4) / 6;

      setLength(xyValues, 2, length(xyValues[0]) + 1);
      xyValues[1][i + 1] := yi + _h * m;
      xyValues[0][i + 1] := xi + _h;

      i := i + 1;
   end;
   Result := xyValues;
end;
end.
